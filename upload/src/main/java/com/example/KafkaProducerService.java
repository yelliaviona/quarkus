package com.example;

import io.smallrye.reactive.messaging.annotations.Channel;
import io.smallrye.reactive.messaging.annotations.Emitter;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Service class for producing messages to a Kafka topic.
 * Uses SmallRye Reactive Messaging to emit messages to the configured channel.
 */
@ApplicationScoped
public class KafkaProducerService {

    @Inject
    @Channel("my-outgoing-channel")
    Emitter<String> emitter;

    /**
     * Sends a message to the Kafka topic via the emitter.
     *
     * @param message the message to be sent
     */
    public void sendMessage(String message) {
        emitter.send(message);
    }
}