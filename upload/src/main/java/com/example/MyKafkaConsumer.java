package com.example;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

/**
 * Service class for consuming messages from a Kafka topic.
 */
public class MyKafkaConsumer {
    private final KafkaConsumer<String, String> consumer;

    /**
     * Constructor to initialize the Kafka consumer with required properties.
     */
    public MyKafkaConsumer() {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("group.id", "my-consumer-group");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        this.consumer = new KafkaConsumer<>(props);
    }

    /**
     * Consumes messages from the specified Kafka topic and prints them.
     *
     * @param topic the Kafka topic to consume messages from
     */
    public void consumeMessages(String topic) {
        consumer.subscribe(Collections.singletonList(topic));
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
            for (ConsumerRecord<String, String> record : records) {
                System.out.println("Received message: " + record.value());
            }
        }
    }

    /**
     * Closes the Kafka consumer.
     */
    public void closeConsumer() {
        consumer.close();
    }
}