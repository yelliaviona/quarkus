package com.example;


import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.reactive.messaging.Incoming;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;


/**
 * REST resource class providing endpoints for handling HTTP requests.
 * Supports JWT-based security and CSV file upload.
 */
@Path("/")
@SecurityScheme(
        scheme = "bearer",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT"
)
public class Resource {

    @Inject
    KafkaProducerService kafkaProducer;

    private final List<String> messages = Collections.synchronizedList(new ArrayList<>());

    /**
     * Endpoint for users with roles "user" or "admin" to get a list.
     *
     * @return response with a message for user and admin
     */
    @GET
    @RolesAllowed({"user","admin"})
    @Path("getList")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getDataList(){
        return Response.ok("Get List for User and Admin").build();
    }

    /**
     * Endpoint for users with role "admin" to delete data.
     *
     * @return response with a delete message for admin only
     */
    @GET
    @RolesAllowed({"admin"})
    @Path("delete")
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteData(){
        return Response.ok("Delete for Admin Only").build();
    }

    /**
     * Endpoint for uploading a CSV file and sending its content to a Kafka topic.
     *
     * @param input multipart form data input containing the CSV file
     * @return response with the content of the uploaded CSV file
     */
    @POST
    @Path("upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public Response uploadCSV(@MultipartForm MultipartFormDataInput input) {
        try {
            InputStream inputStream = input.getFormDataPart("file", InputStream.class, null);

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            if (bufferedReader.ready()) {
                bufferedReader.mark(1);
                if (bufferedReader.read() != 0xFEFF) {
                    bufferedReader.reset();
                }
            }

            CSVReader csvReader = new CSVReader(bufferedReader);
            List<String[]> records = csvReader.readAll();

            StringBuilder response = new StringBuilder("Uploaded CSV File Content:\n");
            StringBuilder data = new StringBuilder();
            for (String[] record : records) {
                response.append(String.join(", ", record)).append("\n");
                data.append("[").append(String.join(", ", record)).append("]");
            }
            kafkaProducer.sendMessage(data.toString());
            return Response.ok(response.toString()).build();
        } catch (IOException | CsvException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Error reading CSV file").build();
        }
    }

    /**
     * Method to receive messages from a Kafka topic and store them in a list.
     *
     * @param message the received message from Kafka
     */
    @Incoming("my-incoming-channel")
    public void receive(String message) {
        messages.add(message);
    }

    /**
     * Endpoint to get the list of received messages from Kafka.
     *
     * @return list of messages received from Kafka
     */
    @GET
    @Path("consumer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getMessages() {
        return messages;
    }
}