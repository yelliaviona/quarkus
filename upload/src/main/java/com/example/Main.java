package com.example;

import io.quarkus.runtime.annotations.QuarkusMain;
import io.quarkus.runtime.Quarkus;

/**
 * Main class to start the Quarkus application.
 */
@QuarkusMain
public class Main {

    /**
     * Main method to run the Quarkus application.
     *
     * @param args command line arguments
     */
    public static void main(String ... args) {
        Quarkus.run(args);
    }
}