INSERT INTO role (id, name)
VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Manager'),
(4, 'Supervisor'),
(5, 'Analyst');