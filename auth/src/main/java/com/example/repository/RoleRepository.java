package com.example.repository;

import com.example.model.Role;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import javax.enterprise.context.ApplicationScoped;

/**
 * Repository class for managing Role entities.
 * Implements PanacheRepository to provide basic CRUD operations.
 */
@ApplicationScoped
public class RoleRepository implements PanacheRepository<Role> {

    /**
     * Finds a Role entity by its name.
     *
     * @param roleName the name of the role to find
     * @return the Role entity with the specified name, or null if not found
     */
    public Role findByRoleName(String roleName) {
        return find("roleName", roleName).firstResult();
    }
}
