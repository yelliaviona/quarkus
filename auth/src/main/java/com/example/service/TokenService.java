package com.example.service;

import com.example.model.Role;
import com.example.repository.RoleRepository;
import io.smallrye.jwt.build.Jwt;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashSet;
import java.util.Set;

/**
 * Service class for generating JWT tokens based on role information.
 */
@Singleton
public class TokenService {

    @Inject
    RoleRepository roleRepository;

    /**
     * Generates a JWT token for the specified role name.
     *
     * @param roleName the name of the role for which to generate a token
     * @return the generated JWT token
     * @throws IllegalArgumentException if the role is not found
     */
    public String generateToken(String roleName) {
        Role role = roleRepository.findByRoleName(roleName);
        if (role == null) {
            throw new IllegalArgumentException("Role not found");
        }

        Set<String> roles = new HashSet<>();
        roles.add(role.getRoleName());

        return Jwt.issuer("auth")
                .subject("upload")
                .groups(roles)
                .expiresAt(System.currentTimeMillis() + 3600) // Token expiration time in milliseconds
                .sign();
    }
}
