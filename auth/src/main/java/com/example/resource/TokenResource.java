package com.example.resource;

import com.example.service.TokenService;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * REST resource class providing an endpoint for token generation.
 */
@Path("/token")
public class TokenResource {

    @Inject
    TokenService tokenService;

    /**
     * Endpoint to generate a token based on the provided role name.
     *
     * @param roleName the name of the role for which to generate a token
     * @return the generated token as a plain text response
     */
    @GET
    @Path("/{roleName}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getToken(@PathParam("roleName") String roleName) {
        return tokenService.generateToken(roleName);
    }
}
